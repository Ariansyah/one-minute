[_tb_system_call storage=system/_Scene5.ks]

[cm  ]
*HaloHalo

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Waktu menunjukan pukul 18.04)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="bedroom.jpg"  ]
[chara_show  name="Erick"  time="1000"  wait="true"  storage="chara/5/ErickNewOutfit.png"  width="1058"  height="1411"  left="-369"  top="-76"  reflect="false"  ]
[tb_start_text mode=1 ]
#Erick
Waduh mau malam..[p]
Beb, ayo kita pulang![p]
[_tb_end_text]

[chara_show  name="Reni"  time="1000"  wait="true"  storage="chara/4/Reni.png"  width="352"  height="1055"  left="634"  top="83"  reflect="false"  ]
[tb_start_text mode=1 ]
#Reni
Iya, ayo![p]
[_tb_end_text]

[chara_show  name="Sheila"  time="1000"  wait="true"  storage="chara/3/shelia.png"  width="996"  height="1328"  left="18"  top="-36"  reflect="false"  ]
[tb_start_text mode=1 ]
#Sheila
Iya nih sudah mau malam![p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="Adi"  time="1000"  wait="true"  storage="chara/2/Adi.png"  width="357"  height="1166"  left="6"  top="35"  reflect="false"  ]
[tb_start_text mode=1 ]
#Adi
Dina, kamu mau aku antarkan pulang tidak?[p]
[_tb_end_text]

[chara_show  name="Dina"  time="1000"  wait="true"  storage="chara/1/Dina_new_outfit.png"  width="1013"  height="1351"  left="354"  top="-28"  reflect="false"  ]
[tb_start_text mode=1 ]
#Dina
Tidak usah, lagian ada [emb exp = "f.nama"] yang mengantarkan aku pulang, ya kan [emb exp = "f.nama"]?[p]
#&[f.nama]
Aku...?![p]
#Adi
Oh ya kalau begitu din, hati - hati di jalan![p]
#Dina
Ya.[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="living_room_.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Kami pun berpamitan kepada Adi dan orang tuanya.)[p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_Jalan.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="pagar.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Aku pun membonceng Dina hingga keluar gerbang rumah Adi.)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="jalan_jalan.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Sesampainya pinggir jalan.)[p]

[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_start_text mode=1 ]
#&[f.nama]
Din...[p]
Aku ada urusan bentar, kamu bisa kan pesan Ojek Online?[p]

[_tb_end_text]

[chara_show  name="Dina"  time="1000"  wait="true"  storage="chara/1/Dina_new_outfit.png"  width="633"  height="844"  left="197"  top="48"  reflect="false"  ]
[tb_start_text mode=1 ]
#Dina
Kamu tuh...[p]
Selalu saja begini, masa kamu tega suruh perempuan pulang sendirian di malam hari![p]
#&[f.nama]
(Aku hanya bisa terdiam melihat Dina marah dan berjalan meninggalkanku.)[p]
[_tb_end_text]

[chara_hide  name="Dina"  time="1000"  wait="true"  pos_mode="true"  ]
[jump  storage="Scene6.ks"  target="*Yo_ayo"  cond=""  ]
[s  ]
