[_tb_system_call storage=system/_Scene2.ks]

[cm  ]
*Lanjutbro

[bg  time="1000"  method="crossfade"  storage="school_corridor_afternoon.jpg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Aku berlari cepat melewati lorong kampus dan menuruni tangga kampus.)[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="evening02.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Akhirnya aku pun sampai di area parkiran.[p]
Dengan nafas tergesa-gesa aku berusaha cepat menghidupkan vespa tuaku.)[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_start_sfx.ogg"  ]
[tb_start_text mode=1 ]
#
[p][p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Namun aku terhenti sejenak saat melihat matahari.)[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="solar_eclipse.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Seperti ada bayangan yang menutupi matahari.)[p]
Ahh mungkin cuman khayalanku saja.[p]
[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_Jalan.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="street_bg_dusk.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Aku pun bergegas ke rumah adi.)[p]...[p]
(Adi adalah teman semasa SMA aku, [p]
Dia adalah bintang futsal di sekolahku dulu, banyak prestasi yang dia cetak dalam bidang olahraga lebih tepatnya futsal.[p]
Jika aku dibandingkan dengan dia maka seperti Bumi dan Langit,[p]...[p]
tapi dia adalah sahabat karibku dari SMA, ia tidak memandangku sebelah mata tidak seperti lainnya.)[p]
(Setelah beberapa menit di perjalanan....[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[bg  time="1000"  method="crossfade"  storage="ruma_adi.png"  ]
[tb_start_text mode=1 ]
#&[f.nama]
Aku pun sampai di rumah Adi.[p][p]
Ohh ya satu lagi, dia juga anak konglomerat, bapaknya memilki perusahaan minyak di Kalimantan, Dia hidup dengan kemewahan.[p]
Andai saja aku berada di posisinya, Hehehe.)[p]
PAK HERMAN !!![p]
[_tb_end_text]

[tb_start_text mode=1 ]
#Pak Herman
Mas [emb exp = "f.nama"] ...[p]
Tunggu yah, saya buka pagar dulu![p]

[_tb_end_text]

[playse  volume="100"  time="1000"  buf="0"  storage="Pagar.ogg"  ]
[tb_start_text mode=1 ]
#
(suara pintu pagar)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[chara_show  name="Herman"  time="1000"  wait="true"  storage="chara/6/Lance_character.png"  width="427"  height="673"  left="321"  top="151"  reflect="false"  ]
[tb_start_text mode=1 ]
#Pak Herman
Monggo mas [emb exp = "f.nama"] .[p]
#&[f.nama]
Adi ada di dalam?[p]
#Pak Herman
Ada mas di dalam, [p]
kebetulan temannya ada juga di dalam.[p]
#&[f.nama]
Oh kalau begitu terima kasih mas, Saya masuk dulu mas![p]
#Pak Herman
Iya, silakan mas![p]

[_tb_end_text]

[chara_hide  name="Herman"  time="1000"  wait="true"  pos_mode="true"  ]
[playse  volume="100"  time="1000"  buf="0"  storage="Vespa_Jalan.ogg"  ]
[bg  time="1000"  method="crossfade"  storage="mansion_afternoon.jpg"  ]
[tb_start_text mode=1 ]
#&[f.nama]
(Rumah Adi begitu luas dan megah yang tak bisa dituturkan oleh kata-kata.[p]
Setiap kali aku memasuki rumahnya pasti selalu terkagum-kagum melihatnya dan aku pun berharap bisa memiliki rumah seperti ini.)[p]
[_tb_end_text]

[stopse  time="1000"  buf="0"  ]
[jump  storage="scene3.ks"  target="*gas"  ]
[s  ]
